# Projeto front-end



## Getting started

Projeto front-end do grupo da parte __________

[requisitos](#requisitos)

## Participantes
- Victor Hugo Rocha
- Amanda Senra
- Matheus Herzog

# informacoes pertinentes

Objeto | Documentação | Categoria
:-------- | :-------: | :------
VScode | <https://docs.microsoft.com/pt-br/visualstudio/windows/?view=vs-2022> | editor de texto 
pyton | <https://docs.microsoft.com/pt-br/visualstudio/python/?view=vs-2022> | internet e web
chrome | <https://support.google.com/chrome/?hl=en#topic=9796470> | browser
firefox	| <https://developer.mozilla.org/pt-BR/docs/Mozilla/Firefox> | browser


# Repositorio de Recursos

- <img src="icon/React-icon.svg.png" heigth="30px" width="30px">  **React**

<https://github.com/topics/react>

<https://stackoverflow.com/search?q=react>

<https://pt-br.reactjs.org/docs/getting-started.html>

- <img src="icon/JavaScript-icon.svg.png" heigth="30px" width="30px">  **JavaScript**

<https://github.com/topics/javascript>

<https://stackoverflow.com/questions/tagged/javascript>

<https://devdocs.io/javascript/>

- <img src="icon/html-icon.svg.png" heigth="30px" width="30px">  **html**

<https://github.com/topics/html>

<https://stackoverflow.com/questions/tagged/html>

<https://www.w3schools.com/html/>

- <img src="icon/css-icon.png" heigth="30px" width="30px">  **CSS**

<https://github.com/topics/css>

<https://stackoverflow.com/questions/tagged/css>

<https://www.w3schools.com/css/>

- <img src="icon/nodejs-icon.svg.png" heigth="30px" width="30px">  **NodeJS**

<https://github.com/topics/nodejs>

<https://stackoverflow.com/search?q=nodejs>

<https://nodejs.org/en/docs/>

- <img src="icon/scrum-icon.jpg" heigth="30px" width="30px">  **Sccrum**

<https://scrumguides.org/docs/scrumguide/v1/Scrum-Guide-Portuguese-BR.pdf>

- <img src="icon/npm-icon.png" heigth="30px" width="30px">  **npm**

<https://github.com/topics/npm>

<https://docs.npmjs.com/>

## <a name="requisitos"> Listagem de requisitos:

### Funcionais:

- [RF1] Permitir o cadastro de novos usuários;
- [RF2] Permitir o cadastro de novos projetos;
- [RF3] Permitir o cadastro de novas tecnologias;
- [RF4] Possibilitar que o usuário realize buscas por projetos;
- [RF5] Possibilitar que o usuário filtre as informações de busca por pessoas;
- [RF6] Possibilitar que o usuário filtre as informações de busca por projetos;
- [RF7] Exibir as informações dos projetos;
- [RF8] Exibir quais tecnologia foram usadas no projeto;
- [RF9] Permitir que o usuário adicione arquivos ao projeto;
- [RF10] Permitir a edição dos projetos;
- [RF11] Permitir a edição dos perfis dos alunos;
- [RF12] Permitir pesquisar por alunos dentro de um projeto
- [RF13] Permitir adicionar novidades do IBMEC

### Não Funcionias:

- [RNF1] Possibilitar que o usuário adicione uma foto ao projeto, com até X (Kb, Mb ou Gb);
- [RNF2] Estar disponível para o usuário 24/7;
- [RNF3] Criptografar a senha do usuário;
- [RNF4] Deve rodar em vários tipos de navegadores;
- [RNF5] Estar hospedado no GitLab Pages;
- [RNF6] Ser elaborado com o React;

